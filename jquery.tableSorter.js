// Bit&Black 2017-2018

(function($) {

    var instances = [];

    $.fn.tableSorter = function(options) {         
        
                
        // Keep chain and add elements to exe function only once
    
        return this.each(function() {
            
            if ($.inArray(JSON.stringify($(this)), instances) == -1) {
                
                sortTable($(this), options);
                
                instances.push(JSON.stringify($(this)));
            }                
        });
    
        
        function sortTable(element, options) {
    
                    
            var hasLoaded = false;
            
            var settings = {
                sortFirst: 0,
                headers: {},
                callbacks: {
                    onsortstart: function() {},
                    onsortfinish: function() {},
                },
                callbackBehaviour: {
                    onsortstart: 'continue'
                }
            };
            
            $.extend(settings, options);
                
                        
            $('> thead th', element)
                .attr('data-sort-function', 'enabled')
                .each(function() {
                    
                    indexElement = $(this).index();
                                        
                    if (settings.hasOwnProperty('headers')) { 
                                       
                        if (settings['headers'].hasOwnProperty(indexElement)) { 
                                             
                            if (settings.headers.hasOwnProperty(indexElement) && settings.headers[indexElement].hasOwnProperty('sorter') && settings.headers[indexElement]['sorter'] == false) { 
                                $(this).attr('data-sort-function', 'disabled');
                            }
                        }
                    }
                    
                    if (indexElement == settings.sortFirst) {
                        $(this).attr('data-sort-first', true);
                    }
                });
            
            $('> thead th[data-sort-function="enabled"]', element)
                .click(function(event, hasLoaded) {
                
                    var indexColumn = $(this).index() + 1;
                    
                    var orderBy = $(this).attr('data-sort-order-by');
                    
                    var sortBy = $(this).attr('data-sort-sort-by');
                    
                    var column = $('> tbody td:nth-child(' + indexColumn + ')', element); 
                    
                    var options = {
                        orderBy: orderBy,
                        sortBy: sortBy
                    };
                    
                                       
                    // Callback 
                                            
                    if (hasLoaded !== true) {
                        if (settings.callbacks.hasOwnProperty('onsortstart') && typeof settings.callbacks.onsortstart == 'function') {
                            settings.callbacks.onsortstart(options);
                        }
                    }


                    // Sort or don't sort in order to setting 

                    if (settings.callbackBehaviour.hasOwnProperty('onsortstart') && settings.callbackBehaviour.onsortstart != 'break') {
                        
                        column
                            .sort(function(a, b) {
                                
                                // Use data-value if exists
                                
                                var aValue = ($(a).attr('data-sort-value')) ? $(a).attr('data-sort-value') : $(a).html();
                                var bValue = ($(b).attr('data-sort-value')) ? $(b).attr('data-sort-value') : $(b).html(); 
                                
                                
                                // Convert to lower case 
                                
                                aValue = aValue.toLowerCase();
                                bValue = bValue.toLowerCase();
                                
                                
                                // Convert string to number
                                                            
                                if (isNumber(aValue)) {
                                    aValue = aValue * 1;
                                }
                                if (isNumber(bValue)) {
                                    bValue = bValue * 1;
                                }
                                    
                                if (orderBy == 'asc') {
                                    order = (aValue < bValue) ? -1 : 1; 
                                }
                                else {
                                    order = (aValue > bValue) ? -1 : 1; 
                                }
                                
                                return order;
                            })
                            .each(function(index) {
                                var $row = $(this).closest('tr'); 
                                $('tbody', element).first().append($row);
                            });

                        
                        // Resort slave elements to their master
                                            
                        $('tr[data-sort-pair-master]', element)
                            .each(function() { 
                                var id = $(this).attr('data-sort-pair-id');
                                var indexMaster = $(this).index() + 1;
                                $('tr[data-sort-pair-slave][data-sort-pair-id="' + id + '"]', element)
                                    .detach()
                                    .insertAfter($(this));
                            });
                    }
                
                    
                    // Change sort setting and arrow
                       
                    if ($(this).attr('data-sort-order-by') == 'asc') {
                        $(this)
                            .attr('data-sort-order-by', 'desc')
                            .toggleArrow('&#9650;');
                    }
                    else {
                        $(this)
                            .attr('data-sort-order-by', 'asc')
                            .toggleArrow('&#9660;');
                    }
                    
                    
                    // Callback 
                    
                    if (settings.callbacks.hasOwnProperty('onsortfinish') && typeof settings.callbacks.onsortfinish == 'function') {
                        settings.callbacks.onsortfinish(options);
                    }
                })
                .css('position', 'relative') 
                .append('<span class="tablesorter-arrow" data-ts="'+Date.now()+'"></span>')
                .find('.tablesorter-arrow')
                .html('&#9660;')
                .css('visibility', 'hidden');


            $(element).sorterready();
        
            return $(element);
        };
        
        function isNumber(n) { 
            return /^-?[\d.]+(?:e-?\d+)?$/.test(n); 
        };        
    }
    
    $.fn.toggleArrow = function(icon) { 
        
        $(this)
            .parents('thead')
            .find('.tablesorter-arrow')
            .css('visibility', 'hidden');

        $(this)
            .find('.tablesorter-arrow')
            .html(icon)
            .css('visibility', 'visible')
            .attr('data-sort-function-active', true);
        
        return this;
    };
    
    $.fn.sorterready = function() { 
        $(this)
            .find('[data-sort-first="true"]')
            .trigger('click', [true]);
    };


}(jQuery));
