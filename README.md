# tableSorter

A small jquery plugin to sort tables

## How to use 

### Init

TableSorter needs to be initialised inside the jQuery ready function:

```javascript
$(document).ready(function() {

    $('#yourtable').tableSorter();

}); 
```

### HTML Tables 

TableSorter needs a classical table including `thead` and `tbody`:

```
<table id="yourtable">
    <thead>
        <tr>
            <th>row10</th>
            <th>row20</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>row11</td>
            <td>row21</td>
        </tr>
        <tr>
            <td>row12</td>
            <td>row22</td>
        </tr>
    </tbody>
</table>
```

### Optional 

#### Sort value

If you want to sort your table differently from your table row values, you only need to add the `data-sort-value` attribute to your table cells. It is possible to change these values dynamically, TableSorter will find them from its own. 

#### Disable columns 

You can disable columns for getting sorted. Init TableSorter like this:

```javascript
$(document).ready(function() { 

    $('#yourtable').tableSorter({
        headers: { 
            0: { 
                sorter: false 
            },
        }
    });

}); 
```

#### Keep rows together

If you want to keep two or more rows together no matter how the sort option is, you only have to add two attributes: the `data-sort-pair-id` and `data-sort-pair-master` respectively `data-sort-pair-slave`. 
Then it should look like this: 

```
<table id="yourtable">
    <thead>
        <tr>
            <th>row10</th>
            <th>row20</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>row11</td>
            <td>row21</td>
        </tr>
        <tr data-sort-pair-id="1" data-sort-pair-master>
            <td>row12</td>
            <td>row22</td>
        </tr>
        <tr data-sort-pair-id="1" data-sort-pair-slave>
            <td>row13</td>
            <td>row23</td>
        </tr>
        <tr>
            <td>row14</td>
            <td>row24</td>
        </tr>
    </tbody>
</table>
```

## Help 

Feel free to contact as under hello@bitandblack.com